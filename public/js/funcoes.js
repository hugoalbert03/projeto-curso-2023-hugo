function excluir(id, modulo) {
	if(confirm("Deseja realmente escluir o registro?")) {
		document.location="/"+modulo+"/excluir/"+id;
	}
	return false;
}

function carregarCombo(rota, obj) {
	idMarca = $(obj).val();
	$.ajax({
		type: 'GET',
		url: rota+"/"+idMarca,
		success: function(html){
			$("#combo_modelos").html(html);
		},
		beforeSend: function(){
			$("body").addClass("loading");
	    },
		complete: function () {
			$("body").removeClass("loading");
		},
		error: function(e){
			console.log(e);
		}
	});
	
}