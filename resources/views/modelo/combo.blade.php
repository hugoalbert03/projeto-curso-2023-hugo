<label>Modelo</label>
<select class="form-select" id="id_modelo" name="id_modelo">
	<option value="">Selecione um modelo</option>
	@foreach($modelos as $modelo)
	<option value="{{$modelo->id_modelo}}" {{old('id_modelo') == $modelo->id_modelo ? 'selected' : '' }} >{{$modelo->ds_modelo}}</option>
	@endforeach
</select>
