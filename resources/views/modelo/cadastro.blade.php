@extends('layout/template')
@section('content')
<div class="container">
	<div class="row mt-4 mb-4 linha">
		<div class="col-md-6"><h5>{{isset($modelo) ? 'Editar modelo' : 'Cadastro de modelo'}}</h5></div>
	</div>
	<form method="post" action="{{isset($modelo) ? '/modelo/alterar' : '/modelo/cadastrar'}}">
		@csrf
		<input type="hidden" name="id_modelo" value="{{$modelo->id_modelo ?? ''}}">
		<div class="row">
			<div class="col-md-6">
				<label>Marca</label>
				<select class="form-select" id="id_marca" name="id_marca">
					<option value="">Selecione uma marca</option>
					@foreach($marcas as $marca)
					<option value="{{$marca->id_marca}}" {{isset($modelo) && $modelo->id_marca == $marca->id_marca ? 'selected' : ''}}>{{$marca->ds_marca}}</option>
					@endforeach
				</select>
			</div>
			<div class="col-md-6">
				<label>Modelo</label>
				<input type="text" class="form-control" id="ds_modelo" name="ds_modelo" value="{{$modelo->ds_modelo ?? old('ds_modelo')}}">
			</div>
		</div>
		<div class="row">
			<div class="d-grid gap-2 d-md-flex justify-content-md-end mt-4">
			  	<button class="btn btn-primary me-md-2" type="submit">{{isset($modelo) ? 'Editar' : 'Cadastrar'}}</button>
			  	<button class="btn btn-secondary" type="button" onclick="document.location='/modelo/lista'">Cancelar</button>
			</div>
		</div>
	</form>

</div>   
    
@endsection
