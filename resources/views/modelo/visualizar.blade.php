@extends('layout/template')
@section('content')
<div class="container">
	<div class="row mt-4 mb-4 linha">
		<div class="col-md-6"><h5>Visualizar modelo</h5></div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<label>Marca: </label><br> {{$modelo->marca->ds_marca}}
			
		</div>
		<div class="col-md-6">
			<label>Modelo: </label><br> {{$modelo->ds_modelo}}
			
		</div>
	</div>
	<div class="row">
		<div class="d-grid gap-2 d-md-flex justify-content-md-end mt-4">
		  	<button class="btn btn-secondary" type="button" onclick="document.location='/modelo/lista'">Voltar</button>
		</div>
	</div>

</div>   
    
@endsection
