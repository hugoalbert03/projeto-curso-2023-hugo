@extends('layout/template')
@section('content')
<div class="container">
	<div class="row mt-4 mb-4 linha">
		<div class="col-md-12"><h5>Lista de marcas</h5></div>
	</div>
	<div class="row mt-4 mb-4">
		<div class="col-md-12 d-grid justify-content-md-end">
			<button type="button" class="btn btn-success" onclick="document.location='/marca/cadastro'">Nova marca</button>
		</div>
	</div>
	<table id="lista_marcas" class="table table-striped">
		<thead>
			<tr>
				<th>Código</th>
				<th>Marca</th>
				<th width="100" style="text-align: center">Ações</th>
			</tr>
		</thead>
		<tbody>
			@foreach($marcas as $marca) 
			<tr>
				<td>{{$marca->id_marca}}</td>
				<td>{{$marca->ds_marca}}</td>
				<td align="center">
					<div class="acoes">
						<a href="/marca/visualizar/{{$marca->id_marca}}"><i class="fas fa-search-plus"></i></a>
						<a href="/marca/editar/{{$marca->id_marca}}"><i class="far fa-edit"></i></a>
						<a href="#" onclick="excluir({{$marca->id_marca}}, 'marca')"><i class="far fa-trash-alt"></i></a>
					</div>
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>

</div>  
<script>
new DataTable('#lista_marcas');
</script>    
    
@endsection
