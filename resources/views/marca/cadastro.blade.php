@extends('layout/template')
@section('content')
<div class="container">
	<div class="row mt-4 mb-4 linha">
		<div class="col-md-6"><h5>{{isset($marca) ? 'Editar marca' : 'Cadastro de marca'}}</h5></div>
	</div>
	<form method="post" action="{{isset($marca) ? '/marca/alterar' : '/marca/cadastrar'}}">
		@csrf
		<input type="hidden" name="id_marca" value="{{$marca->id_marca ?? ''}}">
		<div class="row">
			<div class="col-md-6">
				<label>Marca</label>
				<input type="text" class="form-control" id="ds_marca" name="ds_marca" value="{{$marca->ds_marca ?? old('ds_marca')}}">
			</div>
		</div>
		<div class="row">
			<div class="d-grid gap-2 d-md-flex justify-content-md-end mt-4">
			  	<button class="btn btn-primary me-md-2" type="submit">{{isset($marca) ? 'Editar' : 'Cadastrar'}}</button>
			  	<button class="btn btn-secondary" type="button" onclick="document.location='/marca/lista'">Cancelar</button>
			</div>
		</div>
	</form>

</div>   
    
@endsection
