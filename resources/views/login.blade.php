<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <title>{{ env("APP_NAME") }}</title>
    </head>
    <body>
		<div class="container">
			<form method="post" action="/logar">
				<div style="width: 300px; margin: 0 auto; margin-top: 10%; border: 1px solid #999; padding: 20px;border-radius: 15px">
					<div style="text-align: center"><img src="{{ asset('imagens/logo.jpg') }}" width="250" title="Códigos do Bem Veículos"></div>
					<br>
			    	@csrf
			        Login: <br>
			        <input type="text" name="usuario" value="" class="form-control">
			        <br>Senha: <br>
			        <input type="password" name="senha" value="" class="form-control">
			    	<br><br>
			    	<button type="submit" class="btn btn-primary">Autenticar</button>
				    @if(session('mensagem'))
				    <div class="alert alert-danger auto-fechar text-center" style="margin-top: 30px">
				    	<strong>{{session('mensagem')}}</strong>
				    </div>
				    @endif
				</div>
		    </form>
		</div>
	</body>
</html>
