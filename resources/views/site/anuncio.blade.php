<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <link href="{{ asset('css/estilo.css') }}" rel="stylesheet">
        <link href="//cdn.datatables.net/1.13.6/css/jquery.dataTables.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css" integrity="sha512-z3gLpd7yknf1YoNbCzqRKc4qyor8gaKU1qmn+CShxbuBusANI9QpRohGBreCFkKxLhei6S9CQXFEbbKuqLg0DA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        
        <script src="https://code.jquery.com/jquery-3.7.0.js"></script>
        <script src="//cdn.datatables.net/1.13.6/js/jquery.dataTables.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/js/all.min.js" integrity="sha512-uKQ39gEGiyUJl4AI6L+ekBdGKpGw4xJ55+xyJG7YFlJokPNYegn9KwQ3P8A7aFQAUtUsAQHep+d/lrGqrbPIDQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <script src="{{asset('js/funcoes.js?rand=')}}{{rand()}}"></script>
        <title>{{ env("APP_NAME") }}</title>
    </head>
    <body>          
		<div id="container-fluid topo">
			<div id="logo">
				<img src="{{asset('imagens/logo.jpg')}}" width="200">
			</div>
        	<img src="{{asset('imagens/topo.jpg')}}" class="img-fluid" alt="...">
        </div>
        <div class="container" style="margin-top: 20px;margin-bottom: 70px">
	    	<div class="row">
	           	<div class="col-md-6 mb-4">
	           		<div class=" card-anuncio">
	           			<div class="text-center ">
	           				<img src="{{isset($objVeiculo->ds_foto) ? asset('fotos/'.$objVeiculo->ds_foto) : asset('imagens/sem_foto.png')}}" class="img-fluid rounded" alt="...">
	           			</div>
	           			<div class="txt-anuncio">
	           				<div style="color: blue;text-align: center;font-size: 22px;font-weight: bold;">{{$objVeiculo->modelo->ds_modelo}}</div>
	           				<span>Marca: {{$objVeiculo->modelo->marca->ds_marca}}</span><br>
	           				<span>Cor: {{$objVeiculo->ds_cor}}</span><br>
	           				<span>Placa: {{$objVeiculo->ds_placa}}</span>
	           			</div>
	           		</div>
	           	</div>
	        </div>
	        <button class="btn btn-primary" onclick="document.location='/'">Voltar</button>
	    </div>
        <footer class=".navbar-fixed-bottom">
        	<small>&copy; 2023 - Códigos do Bem</small>
        </footer>
    </body>
</html>
