@extends('layout/template')
@section('content')

<div class="container">
	<div class="row mt-4 mb-4 linha">
		<div class="col-md-6"><h5>{{isset($veiculo) ? 'Editar veiculo' : 'Cadastro de veiculo'}}</h5></div>
	</div>
	<form method="post" action="{{isset($veiculo) ? '/veiculo/alterar' : '/veiculo/cadastrar'}}" enctype="multipart/form-data">
		@csrf
		<input type="hidden" name="id_veiculo" value="{{$veiculo->id_veiculo ?? ''}}">
		<div class="row">
			<div class="col-md-6">
				<label>Marca</label>
				<select class="form-select" id="id_marca" name="id_marca" onchange="carregarCombo('/modelo/comboModelos', this)">
					<option value="">Selecione uma marca</option>
					@foreach($marcas as $marca)
					<option value="{{$marca->id_marca}}" {{isset($veiculo) && $veiculo->modelo->marca->id_marca == $marca->id_marca ? 'selected' : ''}}>{{$marca->ds_marca}}</option>
					@endforeach
				</select>
			</div>
			<div class="col-md-6" id="combo_modelos">
				<label>Modelo</label>
				@if(isset($veiculo))
				<select class="form-select" id="id_modelo" name="id_modelo">
					<option value="">Selecione um modelo</option>
					@foreach($modelos as $modelo)
					<option value="{{$modelo->id_modelo}}" {{isset($veiculo) && $veiculo->id_modelo == $modelo->id_modelo ? 'selected' : '' }} >{{$modelo->ds_modelo}}</option>
					@endforeach
				</select>
				@else
				<select class="form-select">
					<option value="">Selecione um modelo</option>
				</select>
				@endif
			</div>
			<div class="col-md-4">
				<label>Cor</label>
				<input type="text" class="form-control" id="ds_cor" name="ds_cor" value="{{$veiculo->ds_cor ?? old('ds_cor')}}">
			</div>
			<div class="col-md-4">
				<label>Ano</label>
				<input type="text" maxlength="4" class="form-control" id="ds_ano" name="ds_ano" value="{{$veiculo->ds_ano ?? old('ds_ano')}}">
			</div>
			<div class="col-md-4">
				<label>Placa</label>
				<input type="text" maxlength="7" class="form-control" id="ds_placa" name="ds_placa" value="{{$veiculo->ds_placa ?? old('ds_placa')}}">
			</div>
			<div class="col-md-6 mt-4">
				<label>Foto</label><br>
				<input type="file" name="foto" id="foto">
			</div>
			@if(isset($veiculo))
			<div class="col-md-12 mt-4">
				<img src="<?php echo asset('/fotos/'.$veiculo->ds_foto) ?>" width="200">
			</div>
			@endif
		</div>
		<div class="row">
			<div class="d-grid gap-2 d-md-flex justify-content-md-end mt-4">
			  	<button class="btn btn-primary me-md-2" type="submit">{{isset($veiculo) ? 'Editar' : 'Cadastrar'}}</button>
			  	<button class="btn btn-secondary" type="button" onclick="document.location='/veiculo/lista'">Cancelar</button>
			</div>
		</div>
	</form>

</div>
    
@endsection
