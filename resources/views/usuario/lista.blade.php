@extends('layout/template')
@section('content')
<div class="container">
	<div class="row mt-4 mb-4 linha">
		<div class="col-md-12"><h5>Lista de usuários</h5></div>
	</div>
	<div class="row mt-4 mb-4">
		<div class="col-md-12 d-grid justify-content-md-end">
			<button type="button" class="btn btn-success" onclick="document.location='/usuario/cadastro'">Novo usuário</button>
		</div>
	</div>
	<table id="lista_usuarios" class="table table-striped">
		<thead>
			<tr>
				<th>Nome</th>
				<th>Login</th>
				<th width="100" style="text-align: center">Ações</th>
			</tr>
		</thead>
		<tbody>
			@foreach($usuarios as $usuario) 
			<tr>
				<td>{{$usuario->nm_usuario}}</td>
				<td>{{$usuario->ds_login}}</td>
				<td align="center">
					<div class="acoes">
						<a href="/usuario/visualizar/{{$usuario->id_usuario}}"><i class="fas fa-search-plus"></i></a>
						<a href="/usuario/editar/{{$usuario->id_usuario}}"><i class="far fa-edit"></i></a>
						<a href="#" onclick="excluir({{$usuario->id_usuario}}, 'usuario')"><i class="far fa-trash-alt"></i></a>
					</div>
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>

</div>  
<script>
new DataTable('#lista_usuarios');
</script>    
    
@endsection
