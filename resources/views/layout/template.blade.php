<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <link href="{{ asset('css/estilo.css') }}" rel="stylesheet">
        <link href="//cdn.datatables.net/1.13.6/css/jquery.dataTables.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css" integrity="sha512-z3gLpd7yknf1YoNbCzqRKc4qyor8gaKU1qmn+CShxbuBusANI9QpRohGBreCFkKxLhei6S9CQXFEbbKuqLg0DA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        
        <script src="https://code.jquery.com/jquery-3.7.0.js"></script>
        <script src="//cdn.datatables.net/1.13.6/js/jquery.dataTables.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/js/all.min.js" integrity="sha512-uKQ39gEGiyUJl4AI6L+ekBdGKpGw4xJ55+xyJG7YFlJokPNYegn9KwQ3P8A7aFQAUtUsAQHep+d/lrGqrbPIDQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <script src="{{asset('js/funcoes.js?rand=')}}{{rand()}}"></script>
        <title>{{ env("APP_NAME") }}</title>
    </head>
    <body>
        <div id="container">           
            <nav class="navbar navbar-expand-sm bg-dark navbar-dark">
			  <div class="container-fluid">
			    <a class="navbar-brand" href="{{route('inicio')}}"><img src="{{ asset('imagens/logo.jpg') }}" width="150" title="Códigos do Bem Veículos"></a>
			    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#collapsibleNavbar">
			      <span class="navbar-toggler-icon"></span>
			    </button>
			    <div class="collapse navbar-collapse" id="collapsibleNavbar">
			      <ul class="navbar-nav">
			        <li class="nav-item">
			          <a class="nav-link" href="{{route('cliente.lista')}}">Cliente</a>
			        </li>
			        <li class="nav-item">
			          <a class="nav-link" href="{{route('marca.lista')}}">Marcas</a>
			        </li>
			        <li class="nav-item">
			          <a class="nav-link" href="{{route('modelo.lista')}}">Modelos</a>
			        </li>
			        <li class="nav-item">
			          <a class="nav-link" href="{{route('veiculo.lista')}}">Veículos</a>
			        </li>
			        <li class="nav-item">
			          <a class="nav-link" href="/sair">SAIR</a>
			        </li>  
			        
			      </ul>
			    </div>
			  </div>
			</nav>
            <div class="container" style="margin-top: 20px">
	           	<div class="row">
		           	@if($errors->any())
					    <div class="col-md-12 alert alert-danger auto-fechar text-center">
					        @foreach($errors->all() as $error)
					            <p><strong>{{ $error }}</strong></p>
					        @endforeach
					    </div>
				   	@endif
				   	
				   	@if(session('mensagem'))
			    	<div class="col-md-12 alert alert-success auto-fechar text-center">
			            <strong>{{session('mensagem')}}</strong>
			        </div>
			        @endif
	           	</div>
	        </div>
            <div>
            	@yield('content')
            </div>
            <footer class=".navbar-fixed-bottom">
            	<small>&copy; 2023 - Códigos do Bem</small>
            </footer>
        </div>
    </body>
</html>
