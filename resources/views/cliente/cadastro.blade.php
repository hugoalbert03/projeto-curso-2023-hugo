@extends('layout/template')
@section('content')
<div class="container">
	<div class="row mt-4 mb-4 linha">
		<div class="col-md-6"><h5>{{isset($cliente) ? 'Editar cliente' : 'Cadastro de Cliente'}}</h5></div>
	</div>
	<form method="post" action="{{isset($cliente) ? '/cliente/alterar' : '/cliente/cadastrar'}}">
		@csrf
		<input type="hidden" name="id_cliente" value="{{$cliente->id_cliente ?? ''}}">
		<div class="row">
			<div class="col-md-6">
				<label>Cliente</label>
				<input type="text" class="form-control" id="ds_cliente" name="ds_cliente" value="{{$cliente->ds_cliente ?? old('ds_cliente')}}">
			</div>
		</div>
		<div class="row">
			<div class="d-grid gap-2 d-md-flex justify-content-md-end mt-4">
			  	<button class="btn btn-primary me-md-2" type="submit">{{isset($cliente) ? 'Editar' : 'Cadastrar'}}</button>
			  	<button class="btn btn-secondary" type="button" onclick="document.location='/cliente/lista'">Cancelar</button>
			</div>
		</div>
	</form>

</div>   
    
@endsection
