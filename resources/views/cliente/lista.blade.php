@extends('layout/template')
@section('content')
<div class="container">
	<div class="row mt-4 mb-4 linha">
		<div class="col-md-12"><h5>Lista de cliente</h5></div>
	</div>
	<div class="row mt-4 mb-4">
		<div class="col-md-12 d-grid justify-content-md-end">
			<button type="button" class="btn btn-success" onclick="document.location='/cliente/cadastro'">Novo cliente</button>
		</div>
	</div>
	<table id="lista_cliente" class="table table-striped">
		<thead>
			<tr>
				<th>Código</th>
				<th>Cliente</th>
				<th width="100" style="text-align: center">Ações</th>
			</tr>
		</thead>
		<tbody>
			@foreach($cliente as $client) 
			<tr>
				<td>{{$client->id_cliente}}</td>
				<td>{{$client->ds_cliente}}</td>
				<td align="center">
					<div class="acoes">
						<a href="/cliente/visualizar/{{$client->id_cliente}}"><i class="fas fa-search-plus"></i></a>
						<a href="/cliente/editar/{{$client->id_cliente}}"><i class="far fa-edit"></i></a>
						<a href="#" onclick="excluir({{$client->id_cliente}}, 'cliente')"><i class="far fa-trash-alt"></i></a>
					</div>
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>

</div>  
<script>
new DataTable('#lista_cliente');
</script>    
    
@endsection
