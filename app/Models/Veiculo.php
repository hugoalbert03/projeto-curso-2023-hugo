<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Veiculo extends Model
{
    use HasFactory;
    
    protected $table = "tb_veiculo";
    protected $primaryKey = "id_veiculo";
    
    public $timestamps = false;
    
    protected $fillable = [
    	'id_modelo',
    	'id_usuario_inc',
    	'ds_cor',
    	'ds_ano',
    	'ds_placa',
    	'ds_foto',
    	'fl_ativo'
    ];
    
    public function modelo() {
    	return $this->belongsTo(Modelo::class, 'id_modelo');
    }
    
    public function usuario() {
    	return $this->belongsTo(Usuario::class, 'id_usuario_inc', 'id_usuario');
    }
}
