<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    use HasFactory;
    protected $table = "tb_cliente";
    protected $primaryKey = "id_cliente";
    
    public $timestamps = false;
    
    protected $fillable = [
    	'ds_cliente',
    	'fl_ativo'
    ];
    
    public function idCliente() {
    	return $this->hasMany(CLiente::class, 'id_cliente');
    }
}
