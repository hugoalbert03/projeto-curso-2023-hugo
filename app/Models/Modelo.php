<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Modelo extends Model
{
    use HasFactory;
    
    protected $table = "tb_modelo";
    protected $primaryKey = "id_modelo";
    
    public $timestamps = false;
    
    protected $fillable = [
    	'ds_modelo',
    	'id_marca',
    	'fl_ativo'
    ];
    
    public function marca() {
    	return $this->belongsTo(Marca::class, 'id_marca');
    }
}
