<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Usuario extends Authenticatable
{
    use HasFactory, Notifiable;
    
    protected $table = "tb_usuario";
    protected $primaryKey = "id_usuario";
    
    public $timestamps = false;
    
    protected $fillable = [
        'nm_usuario',
        'ds_login',
        'ds_senha',
        'fl_ativo'
    ];
    
    protected $hidden = [
        'ds_senha'
    ];
    
    public function getAuthPassword() {
        return $this->ds_senha;
    }
    
}
