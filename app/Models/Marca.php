<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Marca extends Model
{
    use HasFactory;
    
    protected $table = "tb_marca";
    protected $primaryKey = "id_marca";
    
    public $timestamps = false;
    
    protected $fillable = [
    	'ds_marca',
    	'fl_ativo'
    ];
    
    public function modelos() {
    	return $this->hasMany(Modelo::class, 'id_marca');
    }

}
