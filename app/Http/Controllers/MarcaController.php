<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Marca;

class MarcaController extends Controller
{
    //
	private $rules = [
		'ds_marca' => 'required|max:40',
	];
	
	private $messages = [
		'required' => 'Campos obrigatórios não informados.',
		'max'      => 'Limite de caracteres.',
	];
	
	public function listar() {
		$objMarca = Marca::where("fl_ativo", 1)->get();
		
		return view("marca.lista", ["marcas" => $objMarca]);
	}
	
	public function cadastrar() {
		return view("marca.cadastro");
	}
	
	public function store(Request $request) {
		$validar = Validator::make($request->all(), $this->rules, $this->messages);
		if($validar->fails()) {
			return back()->withErrors($validar->errors())->withInput();
		}
		
		Marca::create($request->all());
		return redirect(route('marca.lista'))->with("mensagem", 'Marca cadastrada com sucesso!');
	}
	
	public function editar($idMarca) {
		$objMarca = $this->getUmaMarca($idMarca);
		if($objMarca) {
			return view("marca.cadastro", ["marca" => $objMarca]);
		}
		return redirect(route("marca.lista"))->with("mensagem", "Marca não encontrada!");
	}
	
	public function update(Request $request) {
		$validar = Validator::make($request->all(), $this->rules, $this->messages);
		if($validar->fails()) {
			return back()->withErrors($validar->errors())->withInput();
		}
		$dados = $request->all();
		$objMarca = $this->getUmaMarca($dados['id_marca']);
		$msg = "A marca não existe!";
		if($objMarca) {
			$objMarca->update($dados);
			$msg = "Marca alterada com sucesso!";
		}
		return redirect(route("marca.lista"))->with("mensagem", $msg);
	}
	
	public function view($idMarca) {
		$objMarca = $this->getUmaMarca($idMarca);
		if($objMarca) {
			return view("marca.visualizar", ["marca" => $objMarca]);
		}
		return redirect(route("marca.lista"))->with("mensagem", "Marca não encontrada!");
	}
	
	public function delete($idMarca) {
		$objMarca = $this->getUmaMarca($idMarca);
		$msg = "A marca não existe!";
		if($objMarca) {
			$objMarca->update([
				'fl_ativo' => 0	
			]);
			$msg = "Marca excluída com sucesso!";
		}
		return redirect(route("marca.lista"))->with("mensagem", $msg);
	}
	
	// Exclusão física para fins de conhecimento
	/*
	public function delete($idMarca) {
		$objMarca = $this->getUmaMarca($idMarca);
		$msg = "A marca não existe!";
		if($objMarca) {
			$objMarca->delete();
			$msg = "Marca excluída com sucesso!";
		}
		return redirect(route("marca.lista"))->with("mensagem", $msg);
	}
	*/
	
	private function getUmaMarca($idMarca) {
		$objMarca = Marca::where("id_marca", $idMarca)->first();
		return $objMarca;
	}
	
	
	
}
