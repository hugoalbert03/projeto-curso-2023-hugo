<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Modelo;
use App\Models\Marca;
use Illuminate\Support\Facades\Validator;

class ModeloController extends Controller
{
    //
	private $rules = [
		'id_marca' => 'required',
		'ds_modelo' => 'required|max:80'
	];
	
	private $messages = [
		'required' => 'Campos obrigatórios não informados.',
		'max'      => 'Limite de caracteres.',
	];
    
	public function listar() {
		$objModelo = Modelo::where("fl_ativo", 1)->get();		
		return view("modelo.lista", ["modelos" => $objModelo]);
	}
	
	public function cadastrar() {
		$objMarca = Marca::where("fl_ativo", 1)->get();
		return view("modelo.cadastro", ['marcas' => $objMarca]);
	}
	
	public function store(Request $request) {
		$validar = Validator::make($request->all(), $this->rules, $this->messages);
		if($validar->fails()) {
			return back()->withErrors($validar->errors())->withInput();
		}
		
		Modelo::create($request->all());
		return redirect(route('modelo.lista'))->with("mensagem", 'Modelo cadastrado com sucesso!');
	}
	
	public function view($idModelo) {
		$objModelo = $this->getUmModelo($idModelo);
		if($objModelo) {
			return view("modelo.visualizar", ["modelo" => $objModelo]);
		}
		return redirect(route("modelo.lista"))->with("mensagem", "Modelo não encontrado!");
	}
	
	public function editar($idModelo) {
		$objModelo = $this->getUmModelo($idModelo);
		if($objModelo) {
			$objMarca = Marca::where("fl_ativo", 1)->get();
			return view("modelo.cadastro", ["modelo" => $objModelo, "marcas" => $objMarca]);
		}
		return redirect(route("modelo.lista"))->with("mensagem", "Modelo não encontrado!");
	}
	
	public function update(Request $request) {
		$validar = Validator::make($request->all(), $this->rules, $this->messages);
		if($validar->fails()) {
			return back()->withErrors($validar->errors())->withInput();
		}
		$dados = $request->all();
		$objModelo = $this->getUmModelo($dados['id_modelo']);
		$msg = "O modelo não existe!";
		if($objModelo) {
			$objModelo->update($dados);
			$msg = "Modelo alterado com sucesso!";
		}
		return redirect(route("modelo.lista"))->with("mensagem", $msg);
	}
	
	public function delete($idModelo) {
		$objModelo = $this->getUmModelo($idModelo);
		$msg = "O modelo não existe!";
		if($objModelo) {
			$objModelo->update([
				'fl_ativo' => 0
			]);
			$msg = "Modelo excluído com sucesso!";
		}
		return redirect(route("modelo.lista"))->with("mensagem", $msg);
	}
	
	private function getUmModelo($idModelo) {
		$objModelo = Modelo::where("id_modelo", $idModelo)->first();
		return $objModelo;
	}
	
	public function listarModelos($idMarca) {
		$objModelo = Modelo::where([["fl_ativo", 1],["id_marca", $idMarca]])->get();
		return view("modelo.combo", ['modelos' => $objModelo]);
	}
}
