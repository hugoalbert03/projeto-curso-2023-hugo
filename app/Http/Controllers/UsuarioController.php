<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Usuario;

class UsuarioController extends Controller
{
    //
	private $rules = [
		'nm_usuario' => 'required|max:80',
		'ds_login'   => 'required|max:80|email',
	];
	
	private $messages = [
		'required' => 'Campos obrigatórios não informados.',
		'max'      => 'Limite de caracteres.',
		'email'    => 'E-mail inválido.'
	];
    
	public function listar() {
		$objUsuario = Usuario::where('fl_ativo',1)->get();
		
		return view('usuario.lista',['usuarios' => $objUsuario]);
	}
	
	public function cadastrar() {
		return view('usuario.cadastro');
	}
	
	public function editar($idUsuario) {
		$objUsuario = $this->getUmUsuario($idUsuario);
		if($objUsuario) {
			return view('usuario.cadastro', ['usuario' => $objUsuario]);
		}
		return redirect(route('usuario.lista'))->with("mensagem", 'Usuário não encontrado!');
	}
	
	public function store(Request $request) {
		$validar = Validator::make($request->all(),$this->rules,$this->messages);
		if($validar->fails()) {
			return back()->withErrors($validar->errors())->withInput();
		}
		$dados = $request->all();
		$dados['ds_senha'] = bcrypt(123456);
		Usuario::create($dados);
		return redirect(route('usuario.lista'))->with("mensagem", 'Usuário cadastrado com sucesso!');
	}
	
	public function view($idUsuario) {
		$objUsuario = $this->getUmUsuario($idUsuario);
		if($objUsuario) {
			return view("usuario.visualizar", ['usuario' => $objUsuario]);
		}
		return redirect(route('usuario.lista'))->with("mensagem", 'Usuário não encontrado!');
	}
	
	public function update(Request $request) {
		$validar = Validator::make($request->all(),$this->rules,$this->messages);
		if($validar->fails()) {
			return back()->withErrors($validar->errors())->withInput();
		}
		$dados = $request->all();
		$objUsuario = $this->getUmUsuario($dados['id_usuario']);
		$msg = "Usuário não encontrado!";
		if($objUsuario) {
			$objUsuario->update($dados);
			$msg = "Usuário alterado com sucesso!";
		}
		return redirect(route('usuario.lista'))->with("mensagem", $msg);
	}
	
	public function delete($idUsuario) {
		$objUsuario = $this->getUmUsuario($idUsuario);
		$msg = "Usuário não encontrado!";
		if($objUsuario) {
			$objUsuario->update([
				'fl_ativo' => 0	
			]);
			$msg = "Usuário excluído com sucesso!";
		}
		return redirect(route('usuario.lista'))->with("mensagem", $msg);
	}
	
	private function getUmUsuario($idUsuario) {
		$objUsuario = Usuario::where('id_usuario', $idUsuario)->first();
		return $objUsuario;
	}
}
