<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cliente;
use Illuminate\Support\Facades\Validator;
class ClienteController extends Controller
{
    private $rules = [
		'ds_cliente' => 'required|max:40',
	];
	
	private $messages = [
		'required' => 'Campos obrigatórios não informados.',
		'max'      => 'Limite de caracteres.',
	];
	
	public function listar() {
		$objCliente = Cliente::where("fl_ativo", 1)->get();
		
		return view("cliente.lista", ["cliente" => $objCliente]);
	}
	
	public function cadastrar() {
		return view("cliente.cadastro");
	}
	
	public function store(Request $request) {
		$validar = Validator::make($request->all(), $this->rules, $this->messages);
		if($validar->fails()) {
			return back()->withErrors($validar->errors())->withInput();
		} else{
			Cliente::create($request->all());
			return redirect(route('cliente.lista'))->with("mensagem", 'Cliente cadastrado com sucesso!');
		}
		
		
		dd($validar->fails());
	}
	
	public function editar($idCliente) {
		$cliente = Cliente::find($idCliente);
		$objCliente = $this->getUmaMarca($idCliente);
		if($objCliente) {
			return view("marca.cadastro", ["cliente" => $objCliente]);
		}
		return redirect(route("cliente.lista"))->with("mensagem", "Cliente não encontrada!");
	}
	
	public function update(Request $request) {
		$validar = Validator::make($request->all(), $this->rules, $this->messages);
		if($validar->fails()) {
			return back()->withErrors($validar->errors())->withInput();
		}
		$dados = $request->all();
		$objCliente = $this->getUmaMarca($dados['id_cliente']);
		$msg = "O cliente não existe!";
		if($objCliente) {
			$objCliente->update($dados);
			$msg = "Cliente alterado com sucesso!";
		}
		return redirect(route("cliente.lista"))->with("mensagem", $msg);
	}
	
	public function view($idListaCliente) {
		$showCliente = $this->getContaCliente($idListaCliente);
		if($showCliente) {
			return view('cliente.visualizar', ['cliente' => $showCliente]);
		} else {
			return redirect(route("cliente.lista"))->with("mensagem", "Cliente não encontrado!");
		}
		#dd($showCliente->ds_cliente);
		
	}
	
	public function delete($idCliente) {
		$objCliente = $this->getContaCliente($idCliente);
		$msg = "Cliente excluído com sucesso!";
		/*if($objCliente) {
			$objCliente->update([
				'fl_ativo' => 0	
			]);
			$msg = "CLIENTEexcluída com sucesso!";
		}*/
		$objCliente->delete();
		return redirect(route("cliente.lista"))->with("mensagem", $msg);
	}
	
	// Exclusão física para fins de conhecimento
	/*
	public function delete($idMarca) {
		$objMarca = $this->getUmaMarca($idMarca);
		$msg = "A marca não existe!";
		if($objMarca) {
			$objMarca->delete();
			$msg = "Marca excluída com sucesso!";
		}
		return redirect(route("marca.lista"))->with("mensagem", $msg);
	}
	*/
	
	private function getContaCliente($idCliente) {
		$objMarca = Cliente::where("id_cliente", $idCliente)->first();
		return $objMarca;
	}
}
