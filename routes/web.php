<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AutenticarController;
use App\Http\Controllers\UsuarioController;
use App\Http\Controllers\MarcaController;
use App\Http\Controllers\ModeloController;
use App\Http\Controllers\VeiculoController;
use App\Http\Controllers\SiteController;
use App\Http\Controllers\ClienteController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::view('/', 'login')->name('login');
Route::post('/logar', [AutenticarController::class, 'logar'])->name('logar');
Route::get('/sair', [AutenticarController::class, 'logout'])->name('logout');

Route::get('/anuncio', [SiteController::class, 'getAnuncios'])->name('index');
Route::get('/anuncio/{id}', [SiteController::class, 'getAnuncio'])->name('anuncio');

Route::middleware(['auth'])->group(function() {
	Route::view('/inicio', 'index')->name('inicio');
	
	Route::group(['prefix' => 'usuario'], function() {
		Route::get('/lista', [UsuarioController::class, 'listar'])->name('usuario.lista');
		Route::get('/cadastro', [UsuarioController::class, 'cadastrar'])->name('usuario.cadastro');
		Route::post('/cadastrar', [UsuarioController::class, 'store'])->name('usuario.cadastrar');
		
		Route::get('/editar/{id}', [UsuarioController::class, 'editar'])->name('usuario.editar');
		
		Route::get('/visualizar/{id}', [UsuarioController::class, 'view'])->name('usuario.visulaizar');
		Route::get('/excluir/{id}', [UsuarioController::class, 'delete'])->name('usuario.excluir');
		
		Route::post('/alterar', [UsuarioController::class, 'update'])->name('usuario.alterar');
	});
	
	Route::group(['prefix' => 'marca'], function() {
		Route::get('/lista', [MarcaController::class, 'listar'])->name('marca.lista');
		Route::get('/cadastro', [MarcaController::class, 'cadastrar'])->name('marca.cadastro');
		Route::post('/cadastrar', [MarcaController::class, 'store'])->name('marca.cadastrar');
		
		Route::get('/editar/{id}', [MarcaController::class, 'editar'])->name('marca.editar');
		Route::post('/alterar', [MarcaController::class, 'update'])->name('marca.alterar');
		
		Route::get('/visualizar/{id}', [MarcaController::class, 'view'])->name('marca.visulaizar');
		Route::get('/excluir/{id}', [MarcaController::class, 'delete'])->name('marca.excluir');
	});
	
	Route::group(['prefix' => 'modelo'], function() {
		Route::get('/lista', [ModeloController::class, 'listar'])->name('modelo.lista');
		Route::get('/cadastro', [ModeloController::class, 'cadastrar'])->name('modelo.cadastro');
		Route::post('/cadastrar', [ModeloController::class, 'store'])->name('modelo.cadastrar');
		Route::get('/visualizar/{id}', [ModeloController::class, 'view'])->name('modelo.visulaizar');
		Route::get('/editar/{id}', [ModeloController::class, 'editar'])->name('modelo.editar');
		Route::post('/alterar', [ModeloController::class, 'update'])->name('modelo.alterar');
		Route::get('/excluir/{id}', [ModeloController::class, 'delete'])->name('modelo.excluir');
		
		Route::get('/comboModelos/{id}', [ModeloController::class, 'listarModelos'])->name('modelo.listarModelos');

	});
	
	Route::group(['prefix' => 'veiculo'], function() {
		Route::get('/lista', [VeiculoController::class, 'listar'])->name('veiculo.lista');
		Route::get('/cadastro', [VeiculoController::class, 'cadastrar'])->name('veiculo.cadastro');
		Route::post('/cadastrar', [VeiculoController::class, 'store'])->name('veiculo.cadastrar');
		Route::get('/visualizar/{id}', [VeiculoController::class, 'view'])->name('veiculo.visulaizar');
		Route::get('/editar/{id}', [VeiculoController::class, 'editar'])->name('veiculo.editar');
		Route::post('/alterar', [VeiculoController::class, 'update'])->name('veiculo.alterar');
		Route::get('/excluir/{id}', [VeiculoController::class, 'delete'])->name('veiculo.excluir');
		
		
	});

	Route::group(['prefix' => 'cliente'], function() {
		Route::get('/lista', [ClienteController::class, 'listar'])->name('cliente.lista');
		Route::get('/cadastro', [ClienteController::class, 'cadastrar'])->name('cliente.cadastro');
		Route::post('/cadastrar', [ClienteController::class, 'store'])->name('cliente.cadastrar');
		
		Route::get('/editar/{id}', [ClienteController::class, 'editar'])->name('cliente.editar');
		Route::post('/alterar', [ClienteController::class, 'update'])->name('cliente.alterar');
		
		Route::get('/visualizar/{id}', [ClienteController::class, 'view'])->name('cliente.visualizar');
		Route::get('/excluir/{id}', [ClienteController::class, 'delete'])->name('cliente.excluir');
	});
});
